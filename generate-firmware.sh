#!/bin/sh -e

if [ -z "${1}" ]; then
	echo "ERROR, no vendor directory provided" >&2
	return 1
fi

output="${1}"
fw_src="${output}/firmware-src"

if [ ! -d "${fw_src}" ]; then
	echo "ERROR, firmware-src folder not found at ${output}"
	retunr 2
fi

fw_sqfs="${output}/firmware.squashfs"

if [ -e "${fw_sqfs}" ]; then
	echo "Removing current ${fw_sqfs}"
	rm -f "${fw_sqfs}"
fi

fw_files="${output}/firmware-files"

if [ ! -e  "${fw_files}" ]; then
	echo "ERROR, ${fw_files} not found!"
	return 3
fi

mksquashfs "${fw_src}" "${fw_sqfs}" \
    -pf "${fw_files}" \
    -noappend \
    -no-exports \
    -no-xattrs \
    -comp zstd \
    -Xcompression-level 22 \
    -all-root \
    -mkfs-time 0 \
    -all-time 0 \
    ;
