LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := trail
LOCAL_CATEGORY_PATH := system

TRAIL_SRC_DIR := $(LOCAL_PATH)

$(call load-config)

TARGET_SQUASHFS_BLOCK_SIZE ?= 131072
TARGET_SQUASHFS_COMP ?= xz

pantahub_config := $(firstword $(wildcard $(TARGET_CONFIG_DIR)/pantahub.$(SUBTARGET).config $(TARGET_CONFIG_DIR)/pantahub.config))

# if we are not in a fitimage or have an explicit dtb list config ...

ifneq ("$(TARGET_LINUX_IMAGE_TYPE)","fitimage")

	_DEVICE_TREE_NAMES := $(TARGET_LINUX_DEVICE_TREE_NAMES)

else ifneq ("$(TARGET_LINUX_DEVICE_TREE_NAMES)$(FIT_CONFIGS)","")

	_DEVICE_TREE_NAMES := $(TARGET_LINUX_DEVICE_TREE_NAMES)

        ifeq ("$(FIT_CONFIGS)","")

# invent fit configs based on device tree names hardcoded
                $(foreach f, \
			$(TARGET_LINUX_DEVICE_TREE_NAMES), \
			$(eval _f_norm = $(subst -,__,$(patsubst %.dtb,%,$(notdir $(f))))) \
			$(eval new_FIT_CONFIGS += $(_f_norm)) \
			$(eval FIT_CONFIG_DTB_$(_f_norm) ?= $(notdir $(f))) \
			$(eval FIT_CONFIG_NAME_$(_f_norm) ?= conf-$(patsubst %.dtb,%,$(notdir $(f)))) \
			$(eval new_FIT_CONFIG_NAMES += $(patsubst %.dtb,%,$(notdir $(f)))) \
			$(eval FIT_CONFIG_OVERLAYS_$(_f_norm) := ) \
		)

		FIT_CONFIGS ?= $(new_FIT_CONFIGS)
		FIT_CONFIG_NAMES ?= $(new_FIT_CONFIG_NAMES)

        else

		$(eval echo "You must specify for each of '$(FIT_CONFIGS)' the following fields for manual:\
\
 * FIT_CONFIG_NAME_$config = \"your-config-name-in-its\" \
 * FIT_CONFIG_DTB_$config = \"your-board.dtb\" \
 * FIT_CONFIG_OVERLAYS_$config = \"your-board.dtb\" \
")
        endif

else

# in new fitimage world you would use config/.../fit-config.d/
# for each config, drop a .txt file in that has a space separated list of .dtbs apply.
# first is the base dtb, followed by overlay dtbs.
#
# Example test-machine.txt: test-machine-base.dtb test-machine-camera-overlay.dto test-machine-specialdevice.dto
#

_DEVICE_TREE_NAMES += $(TARGET_LINUX_FIT_IMAGE_CONFIG_DTBS)
_DEVICE_TREE_OVLS += $(TARGET_LINUX_FIT_IMAGE_CONFIG_OVLS)
FIT_CONFIG_NAMES := $(TARGET_LINUX_FIT_IMAGE_CONFIG_NAMES)
FIT_FDT_LOADADDR := $(TARGET_FIT_FDT_LOADADDR)
FIT_RAMDISK_LOADADDR := $(TARGET_FIT_RAMDISK_LOADADDR)

ifeq ("$(FIT_FDT_LOADADDR)","")
$(error You must set FIT_FDT_LOADADDR to support overlays.\nIf you do not want this feature, then do not use fit-config.d, but just TARGET_LINUX_DEVICE_TREE_NAMES in product.mk)
endif

$(foreach f, \
	$(FIT_CONFIG_NAMES), \
	$(eval _f_norm = $(subst -,__,$(patsubst %.txt,%,$(notdir $(f))))) \
	$(eval FIT_CONFIGS += $(_f_norm)) \
	$(eval FIT_CONFIG_NAME_$(_f_norm) := $(patsubst %.txt,%,$(notdir $(f)))) \
	$(eval FIT_CONFIG_DTB_$(_f_norm) := $(notdir $(firstword $(shell cat $(TARGET_CONFIG_DIR)/fit-config.d/$(f))))) \
	$(eval FIT_CONFIG_OVERLAYS_$(_f_norm) := $(notdir $(wordlist 2, $(words $(shell cat $(TARGET_CONFIG_DIR)/fit-config.d/$(f))),$(shell cat $(TARGET_CONFIG_DIR)/fit-config.d/$(f))))) \
)
endif

ifdef TARGET_FIT_CONFIG_DEFAULT
	FIT_CONFIG_DEFAULT := $(TARGET_FIT_CONFIG_DEFAULT)
else
	FIT_CONFIG_DEFAULT := $(FIT_CONFIG_NAME_$(firstword $(FIT_CONFIGS)))
endif

# by default we get the dtbs fromi fit-config.d/ - assuming such exist
# Build
trail:
	@echo "Using pvr to create factory trail step"
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/objects
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/trails
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/vendor
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/boot
	@if [ -e $(TARGET_VENDOR_DIR)/addons ]; then \
		rm -f $(TARGET_OUT_TRAIL_STAGING)/addon-* $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*; \
		if ls $(TARGET_VENDOR_DIR)/addons/*.*z4 &> /dev/null; then \
			for a in $(TARGET_VENDOR_DIR)/addons/*z4; do \
				addon=`basename $$a`; \
				mkdir -p $(TARGET_OUT_TRAIL_STAGING)/bsp; \
				cp -f $$a $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-$$addon; \
			done; \
		fi; \
	fi
	@if [ -e $(TARGET_VENDOR_DIR)/firmware.squashfs ]; then \
		cp -f $(TARGET_VENDOR_DIR)/firmware.squashfs $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs; \
	elif [ -e $(TARGET_VENDOR_DIR)/firmware-src ] && [ "$(PVR_USE_SRC_BSP)" != "yes" ]; then \
		rm -f $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs; \
		$(TRAIL_SRC_DIR)/generate-firmware.sh $(TARGET_VENDOR_DIR); \
		cp -f $(TARGET_VENDOR_DIR)/firmware.squashfs $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs; \
	fi
	@echo "Setting TOP_DIR: $(TOP_DIR)"
	@echo "Installing BSP modules"
	@for mods in $(TARGET_OUT_STAGING)/lib/modules/*; do \
	   mod=`basename $$mods`; \
	   if [ -d $(TARGET_OUT_STAGING)/lib/modules/$$mod/build ]; then \
		set -x; \
		rm -rf $(TARGET_OUT_STAGING)/lib/modules/$$mod/source; \
		rm -rf $(TARGET_OUT_STAGING)/lib/modules/$$mod/build; \
		if [ -n "${PANTAVISOR_MODULES_OWRT}" -a "${PANTAVISOR_MODULES_OWRT}" = yes ]; then \
			echo "Hard Linking modules in flat format for owrt"; \
			sh -c 'cd $(TARGET_OUT_STAGING)/lib/modules/*/; find -name *.ko | while read -r line; do ln -v $$line `basename $$line`; done;'; \
		fi; \
		modpart=$${PVBUILD_MULTI_KERNEL:+_$$mod}; \
		if [ -d $(TARGET_VENDOR_DIR)/modules/$$mod ]; then \
			mksquashfs $$mods $(TARGET_VENDOR_DIR)/modules/$$mod $(TARGET_OUT_TRAIL_STAGING)/modules$$modpart.squashfs -all-root -comp $(TARGET_SQUASHFS_COMP) -b $(TARGET_SQUASHFS_BLOCK_SIZE); \
		else \
			mksquashfs $$mods $(TARGET_OUT_TRAIL_STAGING)/modules$$modpart.squashfs -all-root -comp $(TARGET_SQUASHFS_COMP) -b $(TARGET_SQUASHFS_BLOCK_SIZE); \
		fi; \
		if [ -z "$(TARGET_EARLY_MODULES_CONFIG)" ]; then \
			rm -rf $(TARGET_OUT_STAGING)/lib/modules/$$mod; \
		fi; \
	   fi; \
	done
	@echo "Addons: `ls $(TARGET_VENDOR_DIR)/addons/*.*z4 ls $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*.*z4 2>/dev/null`"
	@echo "Device tree names: $(_DEVICE_TREE_NAMES) ::"
	@echo "Device Tree Fit Configs: $(FIT_CONFIGS) ::"
	@TOP_DIR=$(TOP_DIR) \
		PV_ADDONS="`ls $(TARGET_OUT_TRAIL_STAGING)/addon-*.*z4 $(TARGET_OUT_TRAIL_STAGING)/bsp/addon-*.*z4 2>/dev/null`" \
		TARGET_OUT_BUILD=$(TARGET_OUT_BUILD) \
		FDT_DTB="$(_DEVICE_TREE_NAMES)" \
		FDT_DTO="$(_DEVICE_TREE_OVLS)" \
		FDT_FIT_CONFIGS="$(FIT_CONFIGS)" \
		FDT_FIT_CONFIG_DEFAULT="$(FIT_CONFIG_DEFAULT)" \
		$(foreach c, $(FIT_CONFIGS), \
			FDT_FIT_CONFIG_NAME_$(c)="$(FIT_CONFIG_NAME_$(c))" \
			FDT_FIT_CONFIG_DTB_$(c)="$(FIT_CONFIG_DTB_$(c))" \
			FDT_FIT_CONFIG_OVERLAYS_$(c)="$(FIT_CONFIG_OVERLAYS_$(c))") \
		TARGET_ARCH="$(TARGET_ARCH)" \
		PV_FIT_SIG_DIR=$(PVBUILD_FIT_SIG_DIR) \
		PV_FIT_SIG_KEY=$(PVBUILD_FIT_SIG_KEY) \
		TARGET_FIT_SIGN_ALG="$(TARGET_FIT_SIGN_ALG)" \
		FIT_LOADADDR="$(TARGET_FIT_LOADADDR)" \
		FIT_RAMDISK_LOADADDR="$(TARGET_FIT_RAMDISK_LOADADDR)" \
		FIT_FDT_LOADADDR="$(FIT_FDT_LOADADDR)" \
		LINUX_TYPE="$(TARGET_LINUX_IMAGE_TYPE)" \
		PVR_X5C_PATH=$(PVR_X5C_PATH) \
		PVR_SIG_KEY=$(PVR_SIG_KEY) \
		PV_BOOT_FILES_FROM_OUT="$(PVBUILD_BOOT_FILES_FROM_OUT)" \
		INITRD_CONFIG=$(INITRD_CONFIG) \
		\
		fakeroot $(TRAIL_SRC_DIR)/pvrfactory \
			$(TARGET_OUT_TRAIL_STAGING) \
			$(TARGET_OUT_TRAIL_STAGING_PRE) \
			$(TARGET_OUT_TRAIL_FINAL) \
			user1 user1
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/boot/
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/vendor/
	@echo -ne "pv_rev=0\0" > $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt
	@if test ! -z "$(TARGET_FDT_FILE)"; then echo -ne "pv_fdtfile=$(TARGET_FDT_FILE)\0" >> $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt; fi
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/config/
	@if [ ! -e $(pantahub_config) ]; then echo "ERROR: No pantahub.config file in $(TARGET_CONFIG_DIR) - $(pantahub_config), must exist before building trail"; exit 1; fi
	if [ -d $(TARGET_VENDOR_DIR)/trailskel ]; then cp -rvf $(TARGET_VENDOR_DIR)/trailskel/* $(TARGET_OUT_TRAIL_FINAL); fi
	@cp $(pantahub_config) $(TARGET_OUT_TRAIL_FINAL)/config/pantahub.config

include $(BUILD_CUSTOM)
